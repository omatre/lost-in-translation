import React from 'react';
import { useSelector } from 'react-redux';
import AlphaToSign from './AlphaToSign';

const TranslationList = function () {
    const translations = useSelector(state => state.translations);

    if (translations.length === 0) {
        return null;
    }

    return (
        translations.map((translation, index, array) => (
            <div className="row">
                <div className="col">
                    {array[array.length - 1 - index] }
                </div>
                <div className="col">
                    <AlphaToSign letters={array[array.length - 1 - index]} isUserPage={true}/>
                </div>
            </div>
        ))
    );
}

export default TranslationList;