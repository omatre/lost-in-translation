import React from "react";

const AlphaToSign = function (textString, userPage) {
    const isUserPage = userPage.isUserPage;
    const letters =
        typeof textString.letters == "string" ? textString.letters.split("") : [];

    //Resize sign-images to fit in container
    const resizeImage = (length) => {
        if (length <= 7) {
            return `${100/length}%`;
        } else if (length <= 16) {
            return `${80/7}%`;
        } else if (length <= 36) {
            return `${80/10}%`;
        } else if (length <= 40) {
            return `${80/13}%`;
        }
    }

    //Subcomponents with sign-images
    let symbols = null;

    if (letters.length === 0) {
        const placeholder = ['s', 'i', 'g', 'n', 's'];
        symbols = placeholder.filter(letter => /[a-zA-Z]/.test(letter)).map((letter, index, array) => (
            <img
                style={{height: "auto", maxWidth: resizeImage(array.length), opacity: "0.3"}}
                className="img-responsive"
                key={index}
                src={require(`../images/sign-symbols/${letter.toLowerCase()}.png`)}
                alt={letter}
            />
        ));
    } else if (isUserPage) {
        symbols = letters.filter(letter => /[a-zA-Z]/.test(letter)).map((letter, index, array) => (
            <img
                style={{height: "auto"}}
                className="img-responsive"
                src={require(`../images/sign-symbols/${letter.toLowerCase()}.png`)}
                alt={letter}
            />
        ));
    } else {
        symbols = letters.filter(letter => /[a-zA-Z]/.test(letter)).map((letter, index, array) => (
            <img
                style={{height: "auto", maxWidth: resizeImage(array.length)}}
                className="img-responsive"
                key={index}
                src={require(`../images/sign-symbols/${letter.toLowerCase()}.png`)}
                alt={letter}
            />
        ));
    }

    return symbols;
};

export default AlphaToSign;
