import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { withRouter, Redirect } from 'react-router-dom';
import { setUsernameAction } from '../store/actions/user.actions';
import { setLoginAction } from '../store/actions/login.actions';

const LoginPage = function (props) {
    const username = useSelector(state => state.user);
    const login = useSelector(state => state.login);
    const dispatch = useDispatch();
    const [errorMessage, setErrorMessage] = useState('');

    //Event handlers
    const handleEnter = function (event) {
        if (username.length >= 2) {
            event.target.value = '';
            dispatch(setLoginAction(true));
        }
    };

    const handleChange = function (event) {
        dispatch(setUsernameAction(event.target.value));
        if (username == null || username === undefined) {
            setErrorMessage('No username defined');
        } else if (username.length < 2) {
            setErrorMessage('Minimum 2 characters long');
        } else {
            setErrorMessage('');
        }
    };

    //Style definitions
    const inputGroupStyle = { 
        margin: "35vh auto auto auto", 
        maxWidth: "40vw", 
        minWidth: "300px" 
    };

    //If logged in => go to translator
    return ( 
        <>
          { login && <Redirect to="/translator" /> }
          { !login && 
            
              <div className="input-group" style={inputGroupStyle}>
                <input type="text" className="form-control" placeholder={errorMessage.length === 0 ? "User's name" : errorMessage} 
                aria-label="User's name" aria-describedby="basic-addon2" onChange={handleChange} required minLength="2"/>
                <div className="input-group-append">
                    <button className="btn btn-outline-secondary" type="button" onClick={handleEnter}>Register</button>
                </div>
              </div>
          }
       </>
    )
}

export default withRouter(LoginPage);