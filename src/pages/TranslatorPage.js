import React, { useState } from 'react';
import AlphaToSign from '../utils/AlphaToSign';
import { Redirect, withRouter } from 'react-router-dom';
import './TranslatorPage.css';
import { useDispatch, useSelector } from 'react-redux';
import { addTranslationAction } from '../store/actions/translations.actions';

const TranslatorPage = function (props) {
    const login = useSelector(state => state.login);
    const dispatch = useDispatch();
    const [translation, setTranslation] = useState('');

    //Event handlers
    const handleChange = function (event) {    
        setTranslation(event.target.value);
    }

    const handleClick = function (event) {
        dispatch(addTranslationAction(translation));
        setTranslation('');
        event.target.value = '';
    }
    
    //Style definitions
    const containersTextSignsStyle = {
        width: "70vw",
        height: "35vh",
        margin: "auto",
        fontSize: "50px",
        borderRadius: "5px",
        resize: "none",
        backgroundColor: "white" 
    };

    const containerMainTranslationPageStyle = { 
        width: "90vw", 
        height: "90vh", 
        margin: "3vh auto auto auto" 
    };

    //If not logged in => go to LoginPage
    return (
        <>
            { !login && <Redirect to='/'/> }
            { login &&
                <div className="container-fluid" style={containerMainTranslationPageStyle}>
                    <div className="form-group">
                        <div className="row-lg">
                            <div className="col-lg-1"></div>
                            <div className="col-lg-10" style={{margin: "auto"}}>
                                <textarea className="form-control" value={translation} onChange={(event) => handleChange(event)}
                                style={containersTextSignsStyle} maxLength="40" placeholder="Text..."/>
                            </div>
                            <div className="col-lg-1"></div>
                        </div><br/>
                        <div className="row-lg">
                            <div className="col-lg-1"></div>
                            
                            <div className="col-lg-10" style={{margin: "auto"}}>
                                <div className="border" style={containersTextSignsStyle}>
                                    <AlphaToSign letters={translation} isUserPage={false} />
                                </div>
                            </div>
                            <div className="col-lg-1"></div>
                        </div><br/>
                        <button className="btn-primary" type="submit" onClick={(event) => handleClick(event)}
                        style={{width: "20vw", height: "5vh", borderRadius: "2px"}}>Save</button>
                    </div>
                </div>
            }
        </>
    )
}

export default withRouter(TranslatorPage);