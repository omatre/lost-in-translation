import React from 'react';
import TranslationList from '../utils/TranslationList';
import { Redirect, useHistory, withRouter } from 'react-router-dom';
import './UserPage.css';
import { useDispatch, useSelector } from 'react-redux';
import { clearTranslationsAction } from '../store/actions/translations.actions';

const UserPage = function (props) {
    const login = useSelector(state => state.login);
    const dispatch = useDispatch();
    const history = useHistory();

    //Style definitions
    const buttonStyle = {
        margin: "2%",
        borderRadius: "2px",
        width: "30vw",
        height: "7vh",
        fontSize: "auto"
    };
    
    const containerUserPageStyle = {
        width: "70vw",
        margin: "auto auto 2vh auto",
        fontSize: "auto"
    };

    const containerTranslationsStyle = {
        border: "2px solid black", 
        borderRadius: "5px"
    }

    //If not logged in => go to LoginPage
    return (
        <>
            { !login && <Redirect to='/'/> }
            { login &&
                <div className="container" style={containerUserPageStyle}>
                    <div className="btn-group">
                        <button className="btn-success" onClick={() => history.replace('/translator')} style={buttonStyle}>Translator</button>
                        <button className="btn-danger" onClick={() => dispatch(clearTranslationsAction())} style={buttonStyle}>Clear translations</button><br/>
                    </div>
                    <div className="container-fluid" style={containerTranslationsStyle}>
                        <div className="row">
                            <div className="col">Text</div>
                            <div className="col">Signs</div>
                        </div>
                        <TranslationList/>
                    </div>
                </div>
            }
        </>
    )
}

export default withRouter(UserPage);