import { CLEAR_USER, SET_USERNAME } from '../actions/action.types';

const userReducer = (state = '', action = {}) => {
    switch (action.type) {
        case SET_USERNAME:
            return action.payload;
        case CLEAR_USER:
            state = '';
            return state;
        default:
            return state;
    }
}

export default userReducer;