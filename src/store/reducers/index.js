import { combineReducers } from 'redux';
import loginReducer from './login.reducer';
import translationsReducer from './translations.reducer';
import userReducer from './user.reducer';

export default combineReducers({
    user: userReducer,
    translations: translationsReducer,
    login: loginReducer 
});