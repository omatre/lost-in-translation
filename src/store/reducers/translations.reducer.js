import { ADD_TRANSLATION, CLEAR_TRANSLATIONS } from '../actions/action.types';

const translationsReducer = (state = [], action = {}) => {
    switch (action.type) {
        case ADD_TRANSLATION:
            if (state.length === 20)
                return [
                    ...state.slice(1, state + 1),
                    action.payload
                ];
            return [
                ...state,
                action.payload
            ];
        case CLEAR_TRANSLATIONS:
            state = [];
            return state;
        default:
            return state;
    }
}

export default translationsReducer;