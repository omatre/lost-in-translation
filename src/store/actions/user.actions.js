import { CLEAR_USER, SET_USERNAME } from './action.types';

export const setUsernameAction = (username = '') => ({
    type: SET_USERNAME,
    payload: username
});

export const clearUserAction = () => ({
    type: CLEAR_USER
});