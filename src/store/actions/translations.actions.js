import { ADD_TRANSLATION, CLEAR_TRANSLATIONS } from './action.types';

export const addTranslationAction = (translation = '') => ({
    type: ADD_TRANSLATION,
    payload: translation
});

export const clearTranslationsAction = () => ({
    type: CLEAR_TRANSLATIONS
});