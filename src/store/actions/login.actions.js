import { SET_LOGIN } from './action.types';

export const setLoginAction = (login = false) => ({
    type: SET_LOGIN,
    payload: login
});