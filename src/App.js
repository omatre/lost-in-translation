import React from 'react';
import './App.css';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from 'react-router-dom';

import Header from './common/Header';
import LoginPage from './pages/LoginPage';
import TranslatorPage from './pages/TranslatorPage';
import UserPage from './pages/UserPage';

function App() {

  return (
    <Router>
      <div className="App">
        <Header></Header>

        <main>
          <Switch>
            <Route exact path="/" render={() => <LoginPage/>} />
            <Route path="/translator" render={() => <TranslatorPage/>} />
            <Route path="/user" component={() => <UserPage/>}/>
          </Switch>
        </main>
      </div>
    </Router>
  );
}

export default App;
