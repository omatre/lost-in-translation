import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory, withRouter } from 'react-router-dom';
import { setLoginAction } from '../store/actions/login.actions';
import { clearTranslationsAction } from '../store/actions/translations.actions';
import { clearUserAction } from '../store/actions/user.actions';
import './Header.css';

const Header = function (props) {
    const username = useSelector(state => state.user);
    const login = useSelector(state => state.login);
    const dispatch = useDispatch();
    const history = useHistory();

    //Event handlers
    const handleClick = function () {
        if (!window.location.href.includes('user')) {
           history.push('/user'); 
        }
    };

    const handleUnRegister = function () {
        dispatch(clearUserAction());
        dispatch(clearTranslationsAction());
        dispatch(setLoginAction(false));
    };
    
    //Classname definitions
    const navbarClassName = "navbar navbar-light";
    const brandClassName = "nav-brand";

    //Style definitions
    const navbarStyle = {
        backgroundColor: "goldenrod",
        height: "5vw"
    };

    const brandStyle = { 
        cursor: "pointer", 
        marginTop: "auto", 
        marginBottom: "auto" 
    };

    const logoStyle = {
        margin: "0 auto auto auto",
        fontSize: "4rem",
        textAlign: "center",
        height: "5vh",
        lineHeight: "5vh",
        color: "#fcedd8",
        background: "goldenrod",
        fontFamily: "Niconne, cursive",
        fontWeight: "700",
        textShadow: "5px 5px 0px #eb452b, 7px 7px 0px #efa032, 9px 9px 0px #46b59b, 11px 11px 0px #017e7f, 13px 13px 0px #052939, 15px 15px 0px #c11a2b"
    };

    const userIconStyle = {
        width: "3vw", 
        marginRight: "1vw" 
    };

    //Subcomponents depending on logged in
    const LoggedInComponent = function () {
        return (
            <nav className={navbarClassName + "justify-content-between"} style={navbarStyle}>
                <div onClick={handleClick} className={brandClassName}
                style={brandStyle}>
                    <img src={require("../images/sign-symbols/user.png")} alt="User icon" style={userIconStyle}/>
                    {'Welcome, ' + username}
                </div>
                <div onClick={handleUnRegister} className={brandClassName}
                style={{cursor: "pointer"}}>Delete user</div>
            </nav>
        )
    }

    const NotLoggedInComponent = function () {
        return (
            <nav className={navbarClassName} style={navbarStyle}>
                <div className={brandClassName} 
                style={logoStyle}>
                    Lost In Translation
                </div>
            </nav>
        )
    }

    return ( login ? <LoggedInComponent/> : <NotLoggedInComponent/> )
}

export default withRouter(Header);